<?php

$page = new \Page\Page();

$help = new \Helpers\Helper();

$cv_vla = ['mojo','licious','estamos','dentro','vamos','ohsi','exacto','adelante','liber','lijyd'];

$app->post('/acceso', function() use($app, $help) {
  $arr = array();
  $arr[] = strip_tags($help->clean((string) $app->request()->post('username')));
  $arr[] = base64_encode(strip_tags($help->clean((string) $app->request()->post('password'))));
  @session_start();
  for ($i=0; $i < count($arr); $i++) { 
    if(strlen($arr[$i]) == 0 && $arr[$i] == '') {
      $app->flash('status', 'debes llenar todos los campos!');
      $app->flash('username', stripcslashes($arr[0]));
      $app->flash('password', stripcslashes(base64_decode($arr[1])));
      $app->response->redirect($app->urlFor('root_lg', array('u' => $arr[0], 'p' => $arr[1])));
    };
  };
  if(!empty($arr) && count($arr)<3) {
    $_SESSION['login']=1;
    @exit;
  }
});

$app->group('/dashboard', function() use($app, $help) {

  $app->map('/update/basic', function() use($app, $help) {
    $arr = array();
    $arr[] = $help->clean((string) $app->request()->post('titulo_pagina'));
    $arr[] = $help->clean((string) $app->request()->post('meta_tags'));
    $arr[] = $help->clean((string) $app->request()->post('link_chat'));
    $arr[] = $help->clean((string) $app->request()->post('link_social'));
    $arr[] = $help->clean((string) $app->request()->post('telefono'));
    $arr[] = $help->clean((string) $app->request()->post('direccion'));
    $arr[] = $help->clean((string) $app->request()->post('email_contacto'));
    $arr[] = $help->clean((string) $app->request()->post('email_php'));
    $arr[] = $help->clean((string) $app->request()->post('meta_tags'));
    $arr[] = $help->clean((string) $app->request()->post('meta_tags'));
  })->via('GET', 'POST');

  $app->map('/update/social', function() use($app, $help) {
    $arr = array();
    $arr[] = $help->clean((string) $app->request()->post('opengraph'));
    $arr[] = $help->clean((string) $app->request()->post('analitycs'));
    $arr[] = $help->clean((string) $app->request()->post('facebook'));
    $arr[] = $help->clean((string) $app->request()->post('twitter'));
    $arr[] = $help->clean((string) $app->request()->post('youtube'));
  })->via('GET', 'POST');
});

$app->group('/informacion', function() use($app, $help) {

  $app->map('/institucional', function() use($app, $help) {
    $arr = array();
    $arr[] = $help->clean((string) $app->request()->post('titulo'));
    $arr[] = $help->clean((string) $app->request()->post('description'));
  })->via('POST');

  $app->map('/condiciones', function() use($app, $help) {
    $arr = array();
    $arr[] = $help->clean((string) $app->request()->post('titulo'));
    $arr[] = $help->clean((string) $app->request()->post('description'));
  })->via('POST');

  $app->map('/ayudar', function() use($app, $help) {
    $arr = array();
    $arr[] = $help->clean((string) $app->request()->post('titulo'));
    $arr[] = $help->clean((string) $app->request()->post('description'));
  })->via('POST');
});



