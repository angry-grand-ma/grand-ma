<?php

$page = new \Page\Page();

$admin = new Admin();

$page->title = 'Sistema';

$app->notFound(function() use($app) {$app->render('page/404_error.php'); });

$app->get('/', function() use ($app, $page, $admin) {
  $page->description = 'description_to_page';
  $page->image = 'url_to_path';
  $page->author = 'K&N';
  $app->render('page/t-0.php', array('layout'=> 1, 'active' => 1));
})->name('root');

$app->get('/a/e-1', function() use ($app, $page) {
  $page->description = 'description_to_page';
  $page->image = 'url_to_path';
  $page->author = 'K&N';
  $app->render('page/t-1.php', array('layout'=> 3, 'active' => 3, 's_a' => 1));
})->name('admin');

$app->get('/a/e-1/top', function() use ($app, $page) {
  $page->description = 'description_to_page';
  $page->image = 'url_to_path';
  $page->author = 'K&N';
  $app->render('page/t-1-p.php', array('layout'=> 3, 'active' => 3, 's_a' => 1));
})->name('admin');

$app->get('/a/e-1/active', function() use ($app, $page) {
  $page->description = 'description_to_page';
  $page->image = 'url_to_path';
  $page->author = 'K&N';
  $app->render('page/t-1-a.php', array('layout'=> 3, 'active' => 3, 's_a' => 1));
})->name('admin');

$app->get('/a/e-1/active/0', function() use ($app, $page) {
  $page->description = 'description_to_page';
  $page->image = 'url_to_path';
  $page->author = 'K&N';
  $app->render('page/t-1-i.php', array('layout'=> 3, 'active' => 3, 's_a' => 1));
})->name('admin');

$app->get('/a', function() use ($app, $page) {
  $page->description = 'description_to_page';
  $page->image = 'url_to_path';
  $page->author = 'K&N';
  $app->render('page/t-5.php', array('layout'=> 3, 'active' => 3, 's_a' => 0));
})->name('admin');

$app->get('/a/e-1/add', function() use ($app, $page) {
  $page->description = 'description_to_page';
  $page->image = 'url_to_path';
  $page->author = 'K&N';
  $app->render('page/t-3.php', array('layout'=> 3, 'active' => 3));
})->name('admin');

$app->get('/a/e-2', function() use ($app, $page) {
  $page->description = 'description_to_page';
  $page->image = 'url_to_path';
  $page->author = 'K&N';
  $app->render('page/t-4.php', array('layout'=> 3, 'active' => 3, 's_a' => 2));
})->name('admin');

$app->get('/a/e-2/active', function() use ($app, $page) {
  $page->description = 'description_to_page';
  $page->image = 'url_to_path';
  $page->author = 'K&N';
  $app->render('page/t-4-a.php', array('layout'=> 3, 'active' => 3, 's_a' => 2));
})->name('admin');

$app->get('/a/e-2/active/0', function() use ($app, $page) {
  $page->description = 'description_to_page';
  $page->image = 'url_to_path';
  $page->author = 'K&N';
  $app->render('page/t-4-r.php', array('layout'=> 3, 'active' => 3, 's_a' => 2));
})->name('admin');

$app->get('/p/:id', function() use ($app, $page) {
  $page->description = 'description_to_page';
  $page->image = 'url_to_path';
  $page->author = 'K&N';
  $app->render('page/t-2.php', array('layout'=> 2));
})->name('preview');

$app->get('/r/:id+:status+:date+:owner+:r', function($data) use ($app, $page) {
  $page->description = 'description_to_page';
  $page->image = 'url_to_path';
  $page->author = 'K&N';
  echo var_dump($data);
})->name('preview');

$app->map('/acceso', function() use($app) {
  @session_start();
  if(isset($_SESSION['login'])) {
    $app->redirect(URL.'index.php/admin/dashboard');
  } else {
    // code
  }
})
->via('GET')
->name('root_lg');

$app->group('/admin', function() use($app) {
  @session_start();
  if(isset($_SESSION['login'])) {
    
  } else {
   $app->response->redirect($app->urlFor('root_lg'), 303);
  }
}, function() use($app, $page) {
  
});