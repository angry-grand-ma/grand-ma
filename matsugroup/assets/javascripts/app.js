$(document).on('ready', function()
{
 $('.add-ot').on('click', function()
 {
  $('.overlay').fadeIn();
  $('.cxt-ovr').fadeIn();
 });

 $('.close').on('click', function()
 {
  $('.overlay').fadeOut();
  $('.cxt-ovr').fadeOut();
 });

 $('.cxt-ovr-dv2 > div:nth-child(2) > div').on('click', function()
 {
  $(this).css({'border' : '2px solid #59A6FE', 'border-radius' : '5px'})
  .siblings().css({'border' : 'none'});
  var attr_class = $(this).find('div').attr('class');
  $('#indicator-color').attr('class', attr_class);
 });

 $('#by-date').on('click', function(e) {
  e.preventDefault();
  $('.overlay').fadeIn();
  $('.f-fetch-date').fadeIn();
 });

 $('#close-date').on('click', function() {
  $('.overlay').fadeOut();
  $('.f-fetch-date').fadeOut();
 });

 $('.cxt-tsk-1 > div').on('click', function()
 {
  if($(this).attr('id')=='no-click') {
   $(this).css('background','white');
  }else {
   $(this).css('background','#F9F9F9').siblings().css('background', 'none');
  }
 });

 $('.cxt-ovr-dv > div:nth-child(1) > div:nth-child(1)').on('click', function()
 {
  $('.cxt-ovr-dv2').fadeIn();
 });

 $('.cxt-ovr-dv2').on('mouseleave', function(){
  $('.cxt-ovr-dv2').fadeOut();
 });

 $('.show-comments').on('click', function() {
  $('.overlay-com').fadeIn();
 });

 $('.overlay-com').on('click', function() {
  $(this).fadeOut();
 });
 $('.cxt-ovr').on('click', function() {
  return false;
 });
 $('.ls-tbl-8').click(function() {
  $('.cxt-adm-comment').fadeIn();
 });
});