<?php

error_reporting(E_ALL | E_STRICT);

$app->response->headers->set('Access-Control-Allow-Methods', 'GET, OPTIONS, POST, DELETE, PUT');

$app->response->headers->set('Access-Control-Allow-Origin', '*');

$app->configureMode('production', function () use ($app) {
    $app->config(array(
        'log.enable' => true,
        'debug' => false
    ));
});

$app->configureMode('development', function () use ($app) {
    $app->config(array(
        'log.enable' => false,
        'debug' => true
    ));
});