<?php 

namespace Page {

  class Page {
    
    public $title = '';

    public $type = '';

    public $ga = '';

    public $youtube = '';

    public $facebook = '';

    public $meta_tags = '';
  }
}