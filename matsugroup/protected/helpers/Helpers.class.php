<?php 

namespace Helpers {

  class Helper {
    
    public $errors_arr = [
      1 => 'Error en Peticion',
      2 => 'Valores no definidos',
      3 => 'Error al subir archivo',
      4 => 'Extension no valida',
      5 => 'Tamaño no valido',
      6 => 'No existe el folder'
    ];

    public $extensions_arr = array('.jpg', '.png');

    public function loginIsFail($app=null) {
        return function () use ($app) {
              if (!isset($_SESSION['lg'])) {
                  if(isset($_COOKIE['LoginFail'])) 
                    $app->redirect('http://delujo.pe/index.php/admin/lg/false');
              }
        };
    }

    public function authenticate($app=null) {
        return function () use ($app) {
          @session_start();
           $routes = array('admin', 'dashboard');
              if (!isset($_SESSION['lg'])) {
                  $_SESSION['urlRedirect'] = $app->request()->getPathInfo();
                  $app->flash('error', 'Oops! Debes loguearte');
                  setcookie("UserIp");
                  $ip_user = $_SERVER['REMOTE_ADDR'];
                  $indicator = 0;
                  if(isset($ip_user) && $ip_user!=='::1') {
                    $cookie = $_COOKIE['UserIp'];
                    setcookie("UserIp", $cookie.','.$ip_user);
                    $l = preg_split('/,/', $cookie);
                    for ($i=0; $i < count($l); $i++) { 
                      if($l[$i]==$ip_user) 
                        $indicator=$indicator+1;
                      if($indicator>3) {
                        setcookie("UserIp", " ");
                        setcookie("LoginFail", 1, time()+30);
                        $indicator=0;
                      }
                    }
                  if(isset($_COOKIE['LoginFail'])) $app->redirect('http://delujo.pe/index.php/admin/lg/false');
                  else $app->redirect('http://delujo.pe/index.php/admin/lg'); 
                  }
              }
            // }
        };
    }    

    public function return_json($app=null) {
      return function () use ($app) {
        $app->response->headers->set('Content-Type', 'application/json');
      };
    }

    public function flash_message($k=null, $cxt='') {
      @session_start();
      return function () use ($app) {
        $app->flash($k,$cxt);
      };
    }

    public function getErrorFile($file=null) {
      $arr = array();
      switch ($_FILES[$file]['error']) {
        case UPLOAD_ERR_OK:
          $arr[] = 'UPLOAD_ERR_OK';
          break;
        case UPLOAD_ERR_NO_TMP_DIR:
          $arr[] = 'UPLOAD_ERR_NO_TMP_DIR';
          break;
        case UPLOAD_ERR_INI_SIZE:
          $arr[] = 'UPLOAD_ERR_INI_SIZE';
          break;
        case UPLOAD_ERR_FORM_SIZE:
          $arr[] = 'UPLOAD_ERR_FORM_SIZE';
          break;
        case UPLOAD_ERR_NO_FILE:
          $arr[] = 'UPLOAD_ERR_NO_FILE';
          break;
        default:
          $arr[] = 'INTERNAL ERROR'; 
          break;
        return var_dump($arr);
      };
    }

    public function val($array, $p) {
      $e = '';
      $l = $p;
      for ($i=0; $i < count($array); $i++) { 
        (isset($array) && !is_numeric($array[$i])) ? $l.='%s' : $l.='%d';
        $e.= ", $array[$i]";
      };
      return printf($l.$e);
    }

    public function deuthenticate() {
      @session_start();
      @session_unset();
      @exit;
    }

    public function __getDate($opt=null) {
      if($opt == 'd') {
        $d = date('Y-n-d');
        return $d;
      };
      if($opt == 's') {
        $s = date('h:i:s');
        return $s;
      };
    }

    public function verifyIsGet($app) {
        return function () use ($app) {
            if ($_SERVER['REQUEST_METHOD'] !== 'GET')
                $app->redirect('./');
        };
    }

    public function __login($sql='') {
        try {
        $dbh= db_connect(BD_HOST,BD_USER,BD_PASSWORD,BD_DATABASE);
        $q= $dbh->prepare($sql);
        $q->execute();
        return $q->execute();
        } 
        catch (PDOException $e) {
        return $e->getMessage();
      };
    }

    public function clean($str=null) {
      $str= trim($str);
      if(get_magic_quotes_gpc()) 
        $str= (string) addslashes($str);
      return (string) addslashes($str); 
    }

    public function __uploadFile($file, $path) {
      try {
        if($_FILES['images']) {
          $files= $_FILES['images'];
          $uploads=  array();
          $size= array('max_height' => '120', 'max_width' => '300');
          for ($i=0; $i < count($files); $i++) {
            if (move_uploaded_file($_FILES['tmp_name'][$i], 'assets/uploads'))
              $upload[]= array('link' => '/assets/uploads/', 'name' => $_FILES['tmp_name'][$i]);
            else 
              return false;
          }
          if(count($files) == 0)
            return true;
        }
      }  catch (Exception $e) {
        return $e->getMessage();
      };
    }

    public function verifyIsNull($val) {
        $val;
        $val = ($val) ? true : null;
        return $val;
    }

    public function __deleteFile($file=null, $path='') {
        $file = verifyIsNull($file);
        $path = verifyIsNull($path);
    }

    public function verifyDate() {
        $date = '';
        return $date;
    }
  }
}
