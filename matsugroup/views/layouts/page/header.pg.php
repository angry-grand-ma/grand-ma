<?php $hd = '<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="es-PE" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="es-PE" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="es-PE" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="es-PE"> <!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# blog: http://ogp.me/ns/blog#">
<!-- Desarrollado por K-N Factory @angry-grand-family
*****************************************
******$$*********************************
******$$******$$*************************
******$$*****$$******$$$$******$$********
******$$***$$********$$*$$*****$$********
******$$**$$*********$$**$$****$$********
******$$***$$********$$***$$***$$********
******$$****$$*******$$****$$**$$********
******$$*****$$******$$*****$$*$$********
******$$******$$*****$$******$$$$********
******$$*******$$****$$*******$$$********
*****************************************
*****************************************
*****************************************
-->
' ?>
<?php if($layout==1) {?>
<?= $hd; ?>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>sistema</title>
  <link rel="stylesheet" href="<?php print CSS_PATH ?>app.css">
  <link rel="stylesheet" href="<?php print CSS_PATH ?>font-awesome.css">
</head>
<body>
  <div class="cxt-wrapper">
      <div class="hd-pr">
        <div class="hd-top">
          <div class="hd-top-left">
            <div>
              <a href="#" title="_">MG</a>
            </div>
            <div>
              <a href="#" title="_" class="<?= ($active===1)? 'active': '' ?>">Ordenes</a>
            </div>
            <div>
              <a href="#" title="_" class="<?= ($active===2)? 'active': '' ?>">Pendientes <span class="indicator">23</span></a>
            </div>
            <div>
              <a href="<?= URL.'index.php/a' ?>" title="adminstrador" class="<?= ($active===3)? 'active': '' ?>">Administrador </a>
            </div>
            <div>
              <a href="#" title="_" class="<?= ($active===5)? 'active': '' ?>">Actividad  <span class="indicator" style="background: #009CFF;">23</span></a>
            </div>
          </div>
          <div class="hd-search">
            <input type="search" name="search" value="" placeholder="  buscar">   
            <div>
                <i class="icon-search"> </i>
            </div>        
          </div>
          <div class="hd-top-right">
            <button type="button" style="background:none;"><i class="icon-refresh" style="color: rgb(246, 246, 246); position: relative; top: -2px; font-size: 18px;"></i></button>
          </div>
        </div>
        <div class="hd-bot">
          <div class="hd-bot-left">
            <div>
              <ul>
                <li> 
                  <div> 
                    <img src="<?php print ASSETS_PATH ?>c1.png" alt="">
                  </div> 
                </li>
                <li> 
                  <div>
                     <img src="<?php print ASSETS_PATH ?>c2.png" alt="">
                  </div> 
                </li>
                <li> 
                  <div>
                     <img src="<?php print ASSETS_PATH ?>c3.png" alt="">
                  
                  </div> 
                </li>
              </ul>
            </div>
            <div>
              <ul>
                <li> 
                  <div> <a href="#" title="_"> Por nombre </a>
                  
                  </div> 
                </li>
                <li> 
                  <div> <a href="#" title="_"> por tiempo </a>
                  
                  </div> 
                </li>
                <li> 
                  <div> <a href="#" id="by-date" title="_">Por fecha </a>
                  
                  </div> 
                </li> 
              </ul>
          </div>
          </div>
          <div class="hd-bot-right">
            <button type="button" class="add-ot">EEFF</button>
            <button type="button" class="add-ot">PEXT</button>
            <button type="button" class="add-ot">PINT</button>
          </div>
        </div>
      </div>
<?php } ?>
<?php if($layout==2) {?>
<?= $hd; ?>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>sistema</title>
  <link rel="stylesheet" href="<?php print CSS_PATH ?>preview.css">
  <link rel="stylesheet" href="<?php print CSS_PATH ?>font-awesome.css">
</head>
<body>
  <div class="cxt-wrapper">
    <div class="hd-top">
          <div class="hd-top-left">
            <div>
              <a href="<?= URL ?>" title="link_to">MG</a>
            </div>
            <div>
              <ul>
                <li> 
                  <div> 
                    <img onclick="window.location.href = '<?= URL ?>'" src="<?php print ASSETS_PATH ?>hd1.png" style="cursor:pointer;" alt="">
                  </div> 
                </li>
                <li> 
                  <div>
                     <img src="<?php print ASSETS_PATH ?>hd2.png" alt="">
                  </div> 
                </li>
                <li> 
                  <div style="top: -3px;">
                    <a href="#" title="_"></a>                  
                  </div> 
                </li>
              </ul>
            </div>
          </div>
          <div class="hd-top-left-2">
            <div class="hd-left-crl" style="margin-left: 120px;">
              <div>
                <p>AM</p>
              </div>
              <div>
                <p>TG</p>
              </div>
              <div>
                <p>MM</p>
              </div>
              <div>
                <p>JW</p>
              </div>
            </div>
            <div class="hd-left-txt" style="margin-top: 3px;">
              <div>
                <div>
                  <p>7</p>
                </div>
                <div>
                  <p>days left in design</p>
                </div>
              </div>
              <div>
                <div>
                  <p>53</p>
                </div>
                <div>
                  <p>days to finish</p>
                </div>
              </div>
            </div>
            <div class="hd-left-icn" style="width: 19%;">
                <div>
                  <img src="<?php print ASSETS_PATH ?>hd3.png" alt="">
                </div>
                <div>
                  <img src="<?php print ASSETS_PATH ?>hd4.png" alt="">
                </div>
                <div>
                  <img src="<?php print ASSETS_PATH ?>hd5.png" alt="">
                </div>
              
            </div>
          </div>
          <div class="hd-top-right">
            <button type="button" style="background:none;"><i class="icon-refresh" style="color: rgb(246, 246, 246); position: relative; top: -2px; font-size: 18px;"></i></button>
          </div>
        </div>
<?php } ?>
<?php if($layout==3) {?>
<?= $hd; ?>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>sistema</title>
  <link rel="stylesheet" href="<?php print CSS_PATH ?>employes.css">
  <link rel="stylesheet" href="<?php print CSS_PATH ?>font-awesome.css">
</head>
<body>
  <div class="cxt-wrapper">
    <div class="hd-pr">
        <div class="hd-top">
          <div class="hd-top-left">
            <div>
              <a href="index.html" title="_">MG</a>
            </div>
            <div>
              <a href="<?= URL ?>" title="_" class="<?= ($active===1)? 'active': '' ?>">Ordenes</a>
            </div>
            <div>
              <a href="#" title="_" class="<?= ($active===2)? 'active': '' ?>">Pendientes <span class="indicator">23</span></a>
            </div>
            <div>
              <a href="<?= URL.'index.php/a' ?>" title="adminstrador" class="<?= ($active===3)? 'active': '' ?>">Administrador </a>
            </div>
            <div>
              <a href="#" title="_" class="<?= ($active===5)? 'active': '' ?>">Actividad  <span class="indicator" style="background: #009CFF;">23</span></a>
            </div>
          </div>
          <div class="hd-search">
            <input type="search" name="search" value="" placeholder="  buscar">   
            <div>
                <i class="icon-search"> </i>
            </div>        
          </div>
          <div class="hd-top-right">
            <button type="button" style="background:none;"><i class="icon-refresh" style="color: rgb(246, 246, 246); position: relative; top: -2px; font-size: 18px;"></i></button>
          </div>
        </div>
        <div class="hd-bot">
          <div class="hd-bot-left">
            <div>
              <ul>
                <li> 
                  <div> 
                    <img src="<?php print ASSETS_PATH ?>c1.png" alt="">
                  </div> 
                </li>
                <li> 
                  <div>
                     <img src="<?php print ASSETS_PATH ?>c2.png" alt="">
                  </div> 
                </li>
                <li> 
                  <div>
                     <img src="<?php print ASSETS_PATH ?>c3.png" alt="">
                  
                  </div> 
                </li>
              </ul>
            </div>
            <div>
              <ul>
                <li> 
                  <div> <a href="http://localhost:9090/matsugroup/index.php/a/e-1" title="_" class="<?= ($s_a===1) ? 'active-s' : '' ?>">contactos</a>
                  
                  </div> 
                </li>
                <li> 
                  <div> <a href="#" title="_">personal</a>
                  
                  </div> 
                </li>
                <li> 
                  <div> <a href="http://localhost:9090/matsugroup/index.php/a/e-2" title="_" class="<?= ($s_a===2) ? 'active-s' : '' ?>">ordenes</a>
                  
                  </div> 
                </li> 
              </ul>
          </div>
          </div>
          <div class="hd-bot-right">
            <button type="button" class="add-ot">EEFF</button>
            <button type="button" class="add-ot">PEXT</button>
            <button type="button" class="add-ot">PINT</button>
          </div>
        </div>
      </div>
<?php } ?>