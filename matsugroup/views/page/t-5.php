<?php require dirname(__DIR__).'/layouts/page/header.pg.php'; ?>    
    <div class="cxt-content">
      <div class="cxt-left">
        <ul>
          <li class="ls-content active-emplyes" style="border-top: 1px solid rgb(243, 243, 243);"> 
            <div>
              <a href="" title="">agregar personal</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-plus"></i></p>
              </div>
            </div>
          </li>
          <li class="ls-content"> 
            <div>
              <a href="" title="">listar destacados</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-list-ul"></i></p>
              </div>
            </div>
          </li>
          <li class="ls-content"> 
            <div>
              <a href="" title="">listar activos</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-ok"></i></p>
              </div>
            </div>
          </li>
          <li class="ls-content"> 
            <div>
              <a href="" title="">listar inactivos</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-minus-sign-alt"></i></p>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div class="cxt-right">
        <div class="cxt-content-prp">
          <div class="cxt-cont-1">
            <div class="cxt-cont-std">
              <div class="hd-cont-std">
                <p>Bounjour Maxence</p>
              </div>
              <div class="cxt-cont-dsc">
                <div class="hd-dsc-tbl">
                  <p>Desde tu ultima conección, <strong>hace 10 días:</strong>  </p>
                </div>
                <div class="cxt-dsc-n1">
                  <div>
                  <p>Nueva orden</p>
                  </div>
                  <div>
                    <p>2</p> 
                  </div>
                  <div>
                    <p> contactos editados</p>
                  </div>
                  <div>
                    <p>5</p>
                  </div>
                </div>
                <div class="cxt-dsc-n1">
                  <div>
                  <p>Nueva orden</p>
                  </div>
                  <div>
                    <p>2</p> 
                  </div>
                  <div>
                    <p> contactos editados</p>
                  </div>
                  <div>
                    <p>5</p>
                  </div>
                </div>
                <div class="cxt-dsc-n1">
                  <div>
                  <p>Nueva orden</p>
                  </div>
                  <div>
                    <p>2</p> 
                  </div>
                  <div>
                    <p> contactos editados</p>
                  </div>
                  <div>
                    <p>5</p>
                  </div>
                </div>
              </div>
              <div class="cxt-cnt-bot">
                <div class="hd-cnt-bt">
                  <div class="hd-ls-lk">
                   <p>enlaces rápidos</p>
                  </div>
                  <div class="cxt-links">
                    <div>
                      <a href="#" title="">agregar nueva orden</a> 
                    <a href="#" title=""> agregar contacto</a> 
                    </div>
                    <div>
                     <a href="#" title="">listado de pendientes</a> 
                    <a href="#" title="">agregar nueva orden</a>  
                    </div>
                </div>
                 </div> 
              </div>
            </div>
            <div class="cxt-ls-right">
              <div class="hd-task">
                <div>
                 <p>tasks</p>
                </div> 
                <div>
                  <p> <span>(you have 10 pending tasks)</span>
                <a href="#" title="">view all</a>   <a href="#" title="">add new task</a></p>
                </div>
              </div>
              <ul>
                <li class="ls-cnt-tsk">
                  <div class="cxt-cnt-ts">
                    <div class="cnt-chk">
                      <input type="checkbox" name="" value=""> 
                    </div>  
                    <div class="cxt-tbl-content">
                      <div class="dv-top-ls">
                        <p>llamar al cliente</p> 
                      </div>
                      <div class="dv-bot-ls">
                        <div>
                          <p>by <span>julie ramboux</span> </p>
                        </div>
                        <div>
                          <p>13/02/2014</p>
                        </div>
                        <div>
                          <img src="" alt="">
                        </div>
                        <div>
                          <p>follow up</p>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                </li>
                <li class="ls-cnt-tsk">
                  <div class="cxt-cnt-ts">
                    <div class="cnt-chk">
                      <input type="checkbox" name="" value=""> 
                    </div>  
                    <div class="cxt-tbl-content">
                      <div class="dv-top-ls">
                        <p>llamar al cliente</p> 
                      </div>
                      <div class="dv-bot-ls">
                        <div>
                          <p>by <span>julie ramboux</span> </p>
                        </div>
                        <div>
                          <p>13/02/2014</p>
                        </div>
                        <div>
                          <img src="" alt="">
                        </div>
                        <div>
                          <p>follow up</p>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                </li>
                <li class="ls-cnt-tsk">
                  <div class="cxt-cnt-ts">
                    <div class="cnt-chk">
                      <input type="checkbox" name="" value=""> 
                    </div>  
                    <div class="cxt-tbl-content">
                      <div class="dv-top-ls">
                        <p>llamar al cliente</p> 
                      </div>
                      <div class="dv-bot-ls">
                        <div>
                          <p>by <span>julie ramboux</span> </p>
                        </div>
                        <div>
                          <p>13/02/2014</p>
                        </div>
                        <div>
                          <img src="" alt="">
                        </div>
                        <div>
                          <p>follow up</p>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="cxt-line">
            <div class="cxt-line-l">
              
            </div>
            <div class="cxt-line-r">
              
            </div>
          </div>
          <div class="cxt-cont-2">
            <div>
              
            </div>
            <div>
              
            </div>
            <div>
              
            </div>
          </div>
        </div>
      </div>
    </div>  
  </div>
</body>
</html>