<?php require dirname(__DIR__).'/layouts/page/header.pg.php'; ?>    
    <div class="cxt-content">
      <div class="cxt-left">
        <ul>
          <li class="ls-content" style="border-top: 1px solid rgb(243, 243, 243);"> 
            <div>
              <a href="" title="">agregar personal</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-plus"></i></p>
              </div>
            </div>
          </li>
          <li class="ls-content"> 
            <div>
              <a href="" title="">listar destacados</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-list-ul"></i></p>
              </div>
            </div>
          </li>
          <li class="ls-content"> 
            <div>
              <a href="" title="">listar activos</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-ok"></i></p>
              </div>
            </div>
          </li>
          <li class="ls-content"> 
            <div>
              <a href="" title="">listar inactivos</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-minus-sign-alt"></i></p>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div class="cxt-right">

      </div>
    </div>  
  </div>
</body>
</html>