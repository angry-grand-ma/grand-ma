<?php require dirname(__DIR__).'/layouts/page/header.pg.php'; ?>    
    <div class="cxt-content">
      <div class="cxt-left">
        <ul>
          <li class="ls-content" style="border-top: 1px solid rgb(243, 243, 243);"> 
            <div>
              <a href="<?= URL.'index.php/a/e-1/add' ?>" title="">agregar personal</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-plus"></i></p>
              </div>
            </div>
          </li>
          <li class="ls-content"> 
            <div>
              <a href="<?= URL.'index.php/a/e-1/top' ?>" title="">listar destacados</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-list-ul"></i></p>
              </div>
            </div>
          </li>
          <li class="ls-content"> 
            <div>
              <a href="<?= URL.'index.php/a/e-1/active' ?>" title="">listar activos</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-ok"></i></p>
              </div>
            </div>
          </li>
          <li class="ls-content active-emplyes"> 
            <div>
              <a href="<?= URL.'index.php/a/e-1/active/0' ?>" title="">listar inactivos</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-minus-sign-alt"></i></p>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div class="cxt-right">
        <ul>
          <li class="ls-right">
            <div class="cxt-ls-dv" style="border-top: 2px solid #F95D5D !important;">
              <div class="hd-content">
                  <div class="hd-check">
                    <i class="icon-check-sign"></i>
                  </div>
                <div class="hd-cnt-left">
                  <div class="hd-check-minus">
                    
                  </div>
                  <div class="hd-cont-imag">
                    <div class="hd-image">
                      <img src="" alt="_">
                    </div>  
                  </div>
                  <div class="hd-name">
                    <div class="hd-name-top">
                      <p>Lillie j. gibson</p>
                    </div>
                    <div class="hd-name-bot">
                      <ul>
                        <li class="ls-check"> <i class="icon-ok-circle" style="cursor:default;"></i> </li>
                        <li class="ls-twitter"> <i class="icon-twitter" style="cursor:default;"></i> <span class="list-span">@demoPe</span> </li>
                        <li class="ls-facebook"> <i class="icon-facebook" style="cursor:default;"></i> <span class="list-span">fb.com/angry-grand-pa</span> </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="hd-cnt-right">
                  <ul>
                    <li class="ls-download"> 
                      <i class="icon-download"></i>
                    </li>
                    <li class="ls-text"> 
                      <i class="icon-list-ul"></i>
                    </li>
                    <li class="ls-mail"> 
                      <i class="icon-edit"> </i>
                    </li>
                    <li class="ls-users"> 
                      <i class="icon-user"> </i>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="cxt-descript">
                <div class="cxt-descript-left">
                  <div class="cxt-ds-1">
                    <div>
                      <p> <strong>location:</strong> </p>
                      <p>new york city</p>
                    </div>
                    <div>
                      <p> <strong>phone:</strong></p>
                      <p> 704-3455</p>
                    </div>
                    <div>
                      <p> <strong>email:</strong></p>
                      <p>lillie@telewom.us</p>
                    </div>
                  </div>
                  <div class="cxt-ds-2">
                    <div>
                      <p> <strong>skills:</strong> </p>
                      <p>Java, J2EE, JSP, serviets, struts, spring, JNDI, EJB, web services,JMS, MVC architee...</p>
                    </div>
                    <div>
                      <p><strong>industry domain:</strong> </p>
                      <p>finance <br> informacion technology</p>
                    </div>
                  </div>
                  <div class="cxt-ds-3">
                    <div>
                      <p> <strong>education:</strong></p>
                      <p> MCS-indiana university 2009 <br> BE-indiana university 2009</p>
                    </div>
                    <div>
                      <p> <strong>experience:</strong></p>
                      <p> 3 yrs 1 mnt (Google) <br> 2 yrs 8 mnts(Apptasky.com)</p>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>  
  </div>
</body>
</html>