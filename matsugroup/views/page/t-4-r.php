<?php require dirname(__DIR__).'/layouts/page/header.pg.php'; ?>    
    <div class="cxt-content">
      <div class="cxt-left">
        <ul>
          <li class="ls-content" style="border-top: 1px solid rgb(243, 243, 243);"> 
            <div>
              <a href="<?= URL.'index.php/a/e-2/active' ?>" title="">listar activas</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-plus"></i></p>
              </div>
            </div>
          </li>
          <li class="ls-content active-emplyes"> 
            <div>
              <a href="<?= URL.'index.php/a/e-2/active/0' ?>" title="">listar retrasadas</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-ok"></i></p>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div class="cxt-right">
        <div class="ls-right-prev">
          <ul>
            <li class="ls-tbl-avance">
              <div class="ls-content-tbl">
                <div class="ls-cont-tbl-avance">
                  <div class="ls-tbl-1">
                    <img src="<?php print ASSETS_PATH ?>star-blue.png" alt="">
                  </div>
                  <div class="ls-tbl-2">
                    <input type="checkbox" name="" value="">
                  </div>
                  <div class="ls-tbl-3">
                    <div>
                      <img src="<?php print ASSETS_PATH ?>time.png" alt="">
                    </div>
                    <div>
                      <p>Orden de trabajo demo</p>                      
                    </div>
                  </div>
                  <div class="ls-tbl-3">
                    <div>
                      <img src="<?php print ASSETS_PATH ?>user-gray.png" alt="">
                    </div>
                    <div>
                      <p>melisa olson/wal-mart</p>                      
                    </div>
                  </div>
                  <div class="ls-tbl-5">
                    <div>
                      <p>10-02-2015</p>                      
                    </div>
                    <div>
                      <img src="<?php print ASSETS_PATH ?>calendary.png" alt="">
                    </div>
                  </div>
                  <div class="ls-tbl-6">
                    <p>16D 05H 12'</p>
                  </div>
                  <div class="ls-tbl-7">
                    <p>alan guerrero</p>
                  </div>
                  <div class="ls-tbl-8">
                    <img src="<?php print ASSETS_PATH ?>select-gray.png" alt="">
                  </div>
                </div>
              </div>
            </li>
            <li class="ls-tbl-alert">
              <div class="ls-content-tbl">
                <div class="ls-cont-tbl-alert">
                  <div class="ls-tbl-1">
                    <img src="<?php print ASSETS_PATH ?>star-gray.png" alt="" style="height: auto; width: 24px; margin-left: 6px; margin-top: 3px;">
                  </div>
                  <div class="ls-tbl-2">
                    <input type="checkbox" name="" value="">
                  </div>
                  <div class="ls-tbl-3">
                    <div>
                      <img src="<?php print ASSETS_PATH ?>alert.png" alt="" style="height: 29px; width: auto; margin-top: 0px; margin-left: -2px;">
                    </div>
                    <div>
                      <p>cierre de la orden</p>                      
                    </div>
                  </div>
                  <div class="ls-tbl-3">
                    <div>
                      <img src="<?php print ASSETS_PATH ?>user-gray.png" alt="">
                    </div>
                    <div>
                      <p>melisa olson/wal-mart</p>                      
                    </div>
                  </div>
                  <div class="ls-tbl-5">
                    <div>
                      <p>10-02-2015</p>                      
                    </div>
                    <div>
                      <img src="<?php print ASSETS_PATH ?>calendary.png" alt="">
                    </div>
                  </div>
                  <div class="ls-tbl-6">
                    <p>16D 05H 12'</p>
                  </div>
                  <div class="ls-tbl-7">
                    <p>alan guerrero</p>
                  </div>
                  <div class="ls-tbl-8">
                    <img src="<?php print ASSETS_PATH ?>select-gray.png" alt="">
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>  
  </div>
<script src="<?= SCRIPTS_PATH ?>jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="<?= SCRIPTS_PATH ?>app.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>