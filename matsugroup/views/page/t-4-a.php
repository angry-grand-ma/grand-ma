<?php require dirname(__DIR__).'/layouts/page/header.pg.php'; ?>    
    <div class="cxt-content">
      <div class="cxt-left">
        <ul>
          <li class="ls-content active-emplyes" style="border-top: 1px solid rgb(243, 243, 243);"> 
            <div>
              <a href="<?= URL.'index.php/a/e-2/active' ?>" title="">listar activas</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-plus"></i></p>
              </div>
            </div>
          </li>
          <li class="ls-content"> 
            <div>
              <a href="<?= URL.'index.php/a/e-2/active/0' ?>" title="">listar retrasadas</a>
            </div> 
            <div>
              <div>
                <p><i class="icon-ok"></i></p>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div class="cxt-right">
        <div class="ls-right-prev">
          <ul>
            <li class="ls-asignation">
              <div class="ls-content-tbl">
                <div class="ls-cont-tbl-gray">
                  <div class="ls-tbl-1">
                    <img src="<?php print ASSETS_PATH ?>star-blue.png" alt="">
                  </div>
                  <div class="ls-tbl-2">
                    <input type="checkbox" name="" value="">
                  </div>
                  <div class="ls-tbl-3">
                    <div>
                      <img src="<?php print ASSETS_PATH ?>flag-blue.png" alt="">
                    </div>
                    <div>
                      <p>asignacion de tarea</p>                      
                    </div>
                  </div>
                  <div class="ls-tbl-3">
                    <div>
                      <img src="<?php print ASSETS_PATH ?>user-gray.png" alt="">
                    </div>
                    <div>
                      <p>melisa olson/wal-mart</p>                      
                    </div>
                  </div>
                  <div class="ls-tbl-5">
                    <div>
                      <p>10-02-2015</p>                      
                    </div>
                    <div>
                      <img src="<?php print ASSETS_PATH ?>calendary.png" alt="">
                    </div>
                  </div>
                  <div class="ls-tbl-6">
                    <p>16D 05H 12'</p>
                  </div>
                  <div class="ls-tbl-7">
                    <p>alan guerrero</p>
                  </div>
                  <div class="ls-tbl-8">
                    <img src="<?php print ASSETS_PATH ?>select-gray.png" alt="">
                  </div>
                </div>
              </div>
              <div class="cxt-adm-comment">
                <div class="cxt-dv-adm1">
                  <input type="text" name="" value="" placeholder="Add a comment">
                </div>
                <div class="cxt-comments-adm">
                  <ul>
                    <li class="ls-comments-admin">
                      <div class="ls-com-admin">
                         <div class="ls-photo-1">
                           <img src="<?php print ASSETS_PATH ?>" alt="">
                         </div>
                         <div class="ls-comment-cnt">
                           <div class="hd-cnt-comments">
                             <div>
                               <p>marie ferguson</p>
                             </div>
                             <div>
                               <span>10Feb 2015-12:32</span>
                             </div>
                             <div>
                               <a href="#" title="">Reply</a>
                               <img src="<?php print ASSETS_PATH ?>eliminar.png" alt="eliminar">
                             </div>
                           </div>
                           <div class="cxt-comment-pr">
                             <p>La orden fue asignada el día 10-02-1015 por el personal Marie Ferguson a las 12:32 hrs.
                              Días transcurridos: 01 día, cliente: Alan Guerrero, tiempo de trabajo: 05 días.</p>
                           </div>
                         </div>
                       </div> 
                       <div class="cxt-load">
                         <button type="button">Load More</button>
                       </div>
                    </li>
                  </ul>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>  
  </div>
<script src="<?= SCRIPTS_PATH ?>jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="<?= SCRIPTS_PATH ?>app.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>