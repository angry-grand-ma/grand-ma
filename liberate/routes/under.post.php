<?php

require dirname(__DIR__).'/protected/db.php';

$a = new \User\User();

$page = new \Page\Page();

$help = new \Helpers\Helper();

$cv_vla = ['mojo','licious','estamos','dentro','vamos','ohsi','exacto','adelante','liber','lijyd'];

$app->post('/newsletter', function() use ($app, $a, $cv_vla, $help) {
  @session_start();
  $mail = $help->clean((string) $app->request()->post('mail'));
  $post_cv = $app->request()->post('cv_value');
  $i = (int) rand(100,300).$cv_vla[rand(1,9)];
  $c_val = (isset($post_cv) && !empty($post_cv)) ? $help->clean((string) $post_cv) : '';
  $message = $app->flash('validation', 'Oops, parece que encontramos datos erroneos en tu direccion de email, ayudanos a validar si eres humano, digita este codigo:'.$i.'<br> <input type="text" name="cv_value" placeholder="validación" required>');
  if(!empty($c_val)) {
    if(strtoupper($c_val)===strtoupper($i)) {
      //save on db
    } else $message; 
  } else 
      if(!$a->subscribeNewsletter($mail)) 
        $message; 
}, function() use ($app, $a, $page, $help) {
  $mail = $help->clean((string) $app->request()->post('mail'));
  if(isset($_SESSION['slim.flash']['validation']) == true 
    && count($_SESSION['slim.flash']['validation']) > 0)
    $app->render('page/index.php', array(
      'msg' => $_SESSION['slim.flash']['validation'], 
      'page' => $page, 
      'respond' => array(
        'email' => $mail
        )
      ));
});

$app->post('/publish',function() use ($app, $a, $help) {
  $arr = array();
  $arr[] = $help->clean((string) strip_tags($app->request()->post('publishment')));
  if (count($a->__postedVideo($arr[0]))>0) $app->response->redirect(URL);
  else die('Oops ocurrio un problema');
});

$app->post('/comment/:id', function($id) use($app, $a, $help) {
  $arr = array();
  $id = base64_decode($help->clean($id));
  $arr[] = $help->clean((string) strip_tags($app->request()->post('comment')));
  if(!empty($arr[0])) {
    if (count($a->__posted($arr[0], $id))>0) print "-";
      else die('Oops ocurrio un problema');
  } else die('que pasa compare');
});