<?php

$app->halt(500, function() use ($app) {
    $app->render('lost.php',500);
});

$app->halt(403, function() use ($app) {
    $app->render('lost.php',403);
});

$app->halt(404, function() use ($app) {
    $app->render('lost.php',404);
});