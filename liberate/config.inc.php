<?php

error_reporting(E_ALL | E_STRICT);

if(!defined('VERSION')) define('VERSION', 'v1.0');

if(!defined('URL')) define('URL', 'http://localhost:9090/liberate/');

if(!defined('PURL')) define('PURL', 'http://localhost:9090/liberate/index.php/');

if(!defined('ASSETS_PATH')) define('ASSETS_PATH', URL.'assets/images/');

if(!defined('CSS_PATH')) define('CSS_PATH', URL.'assets/stylesheets/');

if(!defined('SCRIPTS_PATH')) define('SCRIPTS_PATH', URL.'assets/javascripts/');

if(!defined('UPLOADS_PATH')) define('UPLOADS_PATH', 'assets/uploads/');

if(!defined('ERROR_LOG_URI')) define('ERROR_LOG_URI', 'error_log');

if(!defined('BD_HOST')) define('BD_HOST', 'localhost');

if(!defined('BD_USER')) define('BD_USER', 'root');

if(!defined('BD_DATABASE')) define('BD_DATABASE', 'freedom');

if(!defined('BD_PASSWORD')) define('BD_PASSWORD', '');

if(!defined('EMAIL')) define('EMAIL', 'ventas@liberate.pe');

if(!defined('FACEBOOK')) define('FACEBOOK', '');

if(!defined('TITLE_PAGE')) define('TITLE_PAGE', '');

