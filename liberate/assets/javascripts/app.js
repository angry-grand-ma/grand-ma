$(document).on('ready', 
  function() {
  $('.put-comment').on('keydown', 
    function(e) {
      if(e.keyCode==13) {
        var _this = $(this),
            main = _this.parent().parent(), 
            id = _this.data('id'),
            message = _this.val(),
            data_send = new FormData();
        data_send.append('comment', message);
        $.ajax ({type: "POST", 
          url: "http://localhost:9090/liberate/index.php/comment/"+id, 
          data: data_send, 
          processData: false, 
          contentType: false 
        })
        .done(function( data ) {
          if (data=='-') {
            _this.val('');
            _this.css({'background' : 'white', 'color' : 'black'}); 
            main.prepend('<div class="cxt comment" style="border-left:3px solid #00C9FF;"> <header class="hd top-comment-ls"> <!-- <img src="https://d13yacurqjgara.cloudfront.net/users/3005/avatars/normal/allison-sm.png?1403758185" alt=""> --> <p class="l-title">  <code>7736cdc66b6-999</code></p> <p class="l-comment">'+message+'</p> </header><!-- /header --> </div>');
          } else {
            _this.css({'background' : 'none repeat scroll 0% 0% #FF5C5C', 'color' : 'white'}); 
          }
        }); 
      }
  });
  $('.show-more').on('click',
    function(e) {
      $(this).parent().parent().find('.modal').text('Oops ocurrio un error');
      return false;
  });
});