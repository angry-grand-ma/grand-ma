<?php require dirname(__DIR__).'/layouts/page/header.pg.php'; ?>    
    <!-- aca comienza tu codigo -->
    <div class="cxt cxt-body-list">
      <div class="cxt e-l publish">
        <div class="cxt ls-preview-interes">
          <div class="overlay" style="opacity:.7;"> </div>
          <h3>Comer saludable contra depresion.</h3>
          <img src="assets/images/posts/l1.jpg" alt="">
        </div>
        <div class="cxt ls-preview-interes">
          <div class="overlay" style="opacity:.7;"> </div>
          <h3>Comer saludable contra depresion.</h3>
          <img src="assets/images/posts/l2.jpg" alt="">
        </div>    
        <div class="cxt ls-preview-interes">
          <div class="overlay" style="opacity:.7;"> </div>
          <h3>Comer saludable contra depresion.</h3>
          <img src="assets/images/posts/l3.jpg" alt="">
        </div>
        <div class="cxt ls-preview-interes">
          <div class="overlay" style="opacity:.7;"> </div>
          <h3>Comer saludable contra depresion.</h3>
          <img src="assets/images/posts/l4.jpg" alt="">
        </div> 
        <!-- end list -->            
      </div>
      <div class="cxt e-l list-publish-user">
        <header class="hd hd-interes-home">
          <div class="cxt cxt-main-form">
            <div class="hd e-t top-buttons">
              <li style="border: medium none;"><button type="button"><i class="fa fa fa-file-text" style="margin-left: 8px;"></i> <p>publicar un estado</p></button></li>
              <li><button type="button"><i class="fa fa-video-camera" style="margin-left: 13px;"></i><p> subir un video</p></button></li>
              <li><button type="button"><i class="fa fa-font" style="margin-left: 7px;"></i><p> contar una historia</p></button></li>
              <li><button type="button"><i class="fa fa-link" style="margin-left: 7px;"></i><p> compartir un enlace</p></button></li>
              <li><button type="button"><i class="fa fa-facebook" style="margin-left: 7px; font-size: 23px; margin-top: -4px;"></i></button></li>
            </div>
            <form action="<?= PURL.'publish' ?>" method="post" accept-charset="utf-8">
              <input type="text" class="i txt-publish" name="publishment" placeholder="Que deceas publicar?" maxlength='250'>
              <button type="submit" style="position: relative; top: -27px; opacity: 0; cursor: default;width:10px;height:10px;"></button>
            </form>
          </div>
          <div class="cxt p-show-message">
            <p><i class="fa fa-clock-o"></i> <?php print (!empty($sql)) ? 'Viendo historias más recientes · Volver a las historias destacadas' : 'No existen historias publicadas aun - se el primero, publica  estado' ?>:</p>
          </div>
          <div class="cxt list-show-publishment">
            <?php $letter = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q'); ?>
            <?php foreach ($sql as $k) { ?>
              <?php if ($k['type']=='0'): ?>
                <div class="cxt e-r result-date-publishment status" style="margin-top:15px;box-shadow:none;">
                  <header class="cxt top-user-main">
                    <p>by Anomymous <code><?php print $k['id_owner']; ?></code> <span class="date-l">25/08/2014</span></p>
                  </header><!-- /header -->
                  <div class="cxt main-post">
                    <p><?php print $k['cxt']; ?></p>
                  </div>
                  <div class="cxt cxt-show-comments">
                    <?php $id=$k['id']; ?>
                    <?php $p = __fetchAll("SELECT * FROM posted WHERE type=2 AND main_owner='$id' ORDER BY id DESC LIMIT 4"); ?>
                    <?php foreach ($p as $c) { ?>
                      <div class="cxt comment">
                        <header class="hd top-comment-ls">
                          <!-- <img src="https://d13yacurqjgara.cloudfront.net/users/3005/avatars/normal/allison-sm.png?1403758185" alt=""> -->
                          <p class="l-title">  <code><?php print (!is_string($c['id_owner'][0])) ? $c['id_owner'].'-'.$letter[rand(0,9)] : $c['id_owner'].'-'.rand(0,1000) ?></code></p>
                          <p class="l-comment"><?php print $c['cxt'] ?></p>
                        </header><!-- /header -->
                      </div>
                    <?php } ?>
                      <?php if (count(__fetchAll("SELECT * FROM posted WHERE type=2 AND main_owner='$id'"))>5):?>
                        <div class="show-more">
                          <p class="show-more" data-id="$k['id'];">ver todos los comentarios</p>
                        </div>
                      <?php endif ?>
                      <div class="modal">
                        
                      </div>
                  <div class="cxt cxt-request">
                    <input type="text" class="put-comment" name="comment" data-id="<?= base64_encode($k['id']); ?>" value="" placeholder="ingresa tu comentario..."> 
                  </div>      
                  </div>
                </div>
              <?php endif ?>
              <?php if ($k['type']=='1'): ?>
                <div class="cxt e-r result-date-publishment video">
                  <header class="cxt top-user-main">
                    <p>by Anomymous <code><?php print (!is_string($k['id_owner'][0])) ? $k['id_owner'].'-'.$letter[rand(0,9)] : $k['id_owner'].'-'.rand(0,1000) ?></code> <span class="date-l">25/08/2014</span></p>
                  </header><!-- /header -->
                  <div class="cxt main-video">
                    <p class="fa fa-play" onclick="window.open('<?php print $k['video'] ?>');" style="cursor:pointer;"></p>
                    <div class="overlay" style="background: none repeat scroll 0% 0% rgba(0, 0, 0, 0.51); box-shadow: 0px 0px 100px black inset;"> </div>
                    <img src="<?php print $k['path_image']; ?>" alt="">
                  </div>
                  <div class="cxt action-user-request" style="bottom:0;">
                    <div>
                      <button type="button" style="border-right: none !important;"><i class="fa fa-heart"></i></button>
                      <button type="button"><i class="fa fa-comments-o"></i></button>
                      <button type="button" style="border-left:1px solid #E3E3E3 !important;"><i class="fa fa-comment"></i></button>
                    </div>
                  </div>
                </div>
              <?php endif ?>
            <?php } ?>
            <!-- end -->
          </div>
        </header><!-- /header -->
      </div>
      <div class="cxt e-l feed-home">
        <header class="hd e-c header-feed-home">
          <div class="cxt e-l">
            <button type="button" id="button-feed"><i class="fa fa-external-link"></i> Invitar amigos</button>
            <button type="button" id="button-feed" style="margin-left: -9px;"><i class="fa fa-user-plus "></i> Participar del equipo</button>
            <button type="button" id="action-login"><i class="fa fa-user-secret "></i></button>
            <button type="button" id="action-login"><i class="fa fa-user "></i></button>
          </div>
        </header><!-- /header -->
        <div class="cxt e-c cxt-plush">
          <img src="assets/images/publicidad/l1.png" alt="publish">
        </div>
        <div class="cxt e-c top-events">
          <!--           
          <header class="hd hd-top-stories">
            <h3><i class="fa fa-user-plus"></i> Eventos de la comunidad <div class="cxt e-r arrows-home"> <li><i class="fa fa-chevron-circle-left"></i></li> <li><i class="fa fa-chevron-circle-right"></i></li></div></h3>
          </header> --> 
          <div class="cxt e-c ls-events">
            <h2>Noche de grupo 23 <br> <span>Lima - Miraflores</span></h2>
            <div class="overlay" style="background: none repeat scroll 0% 0% rgba(0, 0, 0, 0.48);height: 98%;"> </div>
            <img src="assets/images/publicidad/l2.jpg" alt="">
          </div>
          <div class="cxt e-c ls-events">
            <h2>Reunion en parque agua <br> <span>Lima - Miraflores</span></h2>
            <div class="overlay" style="background: none repeat scroll 0% 0% rgba(0, 0, 0, 0.48);height: 98%;"> </div>
            <img src="assets/images/publicidad/l3.jpg" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript" src="<?= SCRIPTS_PATH ?>jquery.js"></script>
<script type="text/javascript" src="<?= SCRIPTS_PATH ?>app.js"></script>
</body>
</html>