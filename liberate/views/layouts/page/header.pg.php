<!DOCTYPE html> 
<!--[if lt IE 7 ]> <html lang="es" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="es" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="es" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="es-PE"> <!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# blog: http://ogp.me/ns/blog#">
<!-- Desarrollado por K-N Factory @angry-grand-family
*****************************************
*****************************************
******$$*********************************
******$$******$$*************************
******$$*****$$******$$$$******$$********
******$$***$$********$$*$$*****$$********
******$$**$$*********$$**$$****$$********
******$$***$$********$$***$$***$$********
******$$****$$*******$$****$$**$$********
******$$*****$$******$$*****$$*$$********
******$$******$$*****$$******$$$$********
******$$*******$$****$$*******$$$********
*****************************************
*****************************************
*****************************************
-->
  <meta charset="utf-8"> 
  <title><?= $page->title; ?></title> 
  <meta name="description" content="<?= $page->description; ?>" data-dynamic="true" /> 
  <meta name="author" content="<?= $page->author; ?>" data-dynamic="true" /> 
  <meta property="og:type" content="<?= $page->type; ?>" />
  <meta property="og:url" content="<?="http://[HTTP_HOST][REQUEST_URI]";?>" data-dynamic="true"/>  
  <meta property="og:title" content="<?= $page->title; ?>" /> 
  <meta property="og:image:width" content="250" />
  <meta property="og:image" content="<?= $page->image; ?>"/> 
  <meta property="og:description" content="" /> 
  <!-- <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> -->
  <link href='http://fonts.googleapis.com/css?family=Arimo:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,400italic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">  
  <link href="assets/stylesheets/normalize.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Nothing+You+Could+Do' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Hind:400,500,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="assets/stylesheets/app.css">
</head>
<body>
  <div class="cxt cxt-wrapper e-c">
    <header class="hd top-index">
       <div style="width: 1330px; margin: 0 auto;">
           <nav class="nv nav-list-index e-l">
             <form action="<?= PURL.'find' ?>" method="get" accept-charset="utf-8">
               <input type="search" placeholder="buscar" required autofocus>
               <i class="fa fa-search"></i>
             </form>
           </nav>
           <div class="cxt cxt-upload e-r">
              <form action="<?= URL.'index.php/newsletter' ?>" method="post" accept-charset="utf-8">
                <input type="text" class="input-subscribe" name="mail" placeholder="ingresa tu email">
                <button type="submit"><i class="fa fa-long-arrow-up"></i> Subscribete</button>
              </form>
           </div>
       </div>
    </header><!-- /header -->
