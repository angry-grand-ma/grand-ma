<?php

require 'Slim/Slim.php';

require 'protected/User.class.php';

require 'protected/helpers/Helpers.class.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array(
    'mode' => 'development',
    'cookies.encrypt' => true,
    'cookies.domain' => 'localhost'
));

\Slim\Route::setDefaultConditions(array(
    // 
));

require 'config.inc.php';

require 'includes.php';

require 'routes.php';

$app->run();