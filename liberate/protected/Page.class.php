<?php 

use \Helpers\Helper;

namespace Page {

  class Page {
    
    public $title = '';

    public $type = 'website';

    public $ga = '';

    public $youtube = '';

    public $facebook = '';

    public $meta_tags = '';
  }
}