<?php 

use \Helpers\Helper;

namespace User {

  class User {

    public $id = '2508931203';

    public $protocols = array(
      0 => 'http',
      1 => 'https',
      );

    public $status = array(
      200 => 'on',
      400 => 'off'
      );

    public $hay_stack = array(
       'a', 'b',
       's', 'd',
       'e', 'f',
       'Keys' => array(
        'llolilop', 
        'behappy', 
        'bestrom'
        ),
      );

    protected $host = 'localhost';

    protected $by = 'anonymous';

    private static $scare =  array(
      ':)' => 'fa fa-smile-o', 
      '=)' => 'fa fa-smile-o', 
      'x)' => 'fa fa-smile-o', 
      '3)' => 'fa fa-smile-o'
    );

    private $who;

    public function __construct() {
      return array(
        'accessControl',
        );
    }
    public function subscribeNewsletter($mail='demo@example.com') {
      $i = $this->__randPost();
      $mail = trim($mail);
      $valid = array();
      for ($e=0; $e < strlen($mail); $e++) { 
        if(in_array($mail[$e], $this->hay_stack)) 
          $valid[] = (string) $mail[$e];
      }
     if(count($valid)>4) 
      return 0; 
    }

    protected function getData() {
      // get data to user connect
    }

    public function __randPost() {
      return (is_numeric($this->id)) ? sha1(rand(1000,9000)) : $this->id;
    }

    public function __posted($content=null, $id=null) {
      foreach ($this::$scare as $key => $value) {
        if ($content==$key) $content = '<i class="'.$value.'" style="font-size: 16px; color: #7C7CFF;"></i>';
         else $content = (string) $content;
      }
      $sql = __insert__;
      return $sql;
    } 
    
    public function __postedVideo($url=null) {
      if(!empty($url)) {
        switch ($url) {
          case (strpos($url, 'youtube') == true):
            for ($i=0; $i < strlen($url); $i++) { 
              if($url[$i] === '=') {
                $e = explode('=',$url);
                $m = 'http://img.youtube.com/vi/'.$e[1].'/0.jpg';
                $sql = __insert__;
                return $sql;
                @exit();
              };
            }
            break;
          case (strpos($url, 'youtu.be') == true):
            for ($i=0; $i < strlen($url); $i++) { 
                $e = explode('e/',$url);
                $m = 'http://img.youtube.com/vi/'.$e[1].'/0.jpg';
                $sql = __insert__;
                return $sql;
                @exit();
            }
            break;
          default:
            $s = utf8_encode($url);
            $sql = __insert__;
            return $sql;
            break;
        }
      } else die('que pasa compare');
    }
    
    public function __postedComment($content=null) {
      $i = $this->__randPost();
      $content = (is_string($content) && strlen($content)<250) ? strtolower($content) : null; 
      return array('id' => $i, 'post' => $content);
    }

    public function __postedHistory($content=null, $file=null) {
      $i = $this->__randPost();
      return array('id' => $i, 'post' => $content);
    }
  }  
}
